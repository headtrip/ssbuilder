/**
 * Created by fdrechsler on 04.03.15.
 */
(function () {
    var app = angular.module("ssbuilder", ['ngAnimate']);

    app.controller("MainCtrl", function($scope) {
        $scope.properties = "";

        var initCallback = function(){
           getItems();
        };
        var dataStore = new IDBStore('properties',initCallback);

        var getItemsSuccess = function(data){
            $scope.properties = data;
            // http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
            $scope.$apply();
        };

        var errorCallback = function(){
        };


        var getItems = function(){
            dataStore.getAll(getItemsSuccess,errorCallback);
        };


        var addNewToFrontend = function(property) {
            $scope.properties.push(property);
            $scope.$apply();
        }

        var successCallback = function(id) {
            dataStore.get(id, addNewToFrontend)
        };

        $scope.deleteItem = function(property){
            dataStore.remove(property.id);
            $scope.properties = $scope.properties.filter(function(item) {
                return item.url !== property.url;
            });
            //$scope.$apply();
        }

        $scope.addItem = function(property){
            dataStore.put(property, successCallback);
        };
    });

    app.controller('PropertiesCtrl', function ($scope, $http) {

        this.template = "properties.html";

        this.doAction = function(property, action){
            var d = new Date();
            property.lastUsed = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            property.isRunning = true;

            var responsePromise = $http.get(property.url + action);

            var unsetUsage = function (property) {
                property.isRunning = false;
            };

            responsePromise.success(function (data, status, headers, config) {
                var headerData = headers();

                if(headerData["x-frame-options"] == "SAMEORIGIN") {
                    $.snackbar({content: "FAILED, Are you logged in on " + property.name + "?", timeout: 4000});
                    property.requestFailed = true;
                    property.requestSuccessed = false;
                } else {
                    $.snackbar({content: "Successfully '"+action+"' on " + property.name, timeout: 4000});
                    property.requestFailed = false;
                    property.requestSuccessed = true;
                }

                unsetUsage(property);
            });
            responsePromise.error(function (data, status, headers, config) {
                $.snackbar({content: "FAILED '"+action+"' on " + property.name, timeout: 4000});
                unsetUsage(property);
            });
        }

        this.doFlush = function (property) {
            this.doAction(property, "?flush");
        }

        this.doBuild = function (property) {
            this.doAction(property, "/dev/build");
        }

        this.doBuildFlush = function (property) {
            this.doAction(property, "/dev/build?flush");
        }

        this.getNameIndicator = function(name) {
            return name.substr(0,3);
        }





    });

    app.controller('PropertyCtrl', function ($scope) {

        this.template ="propertyForm.html";
        this.colors = ["red","pink","purple","deeppurple", "indigo", "lightblue",
            "cyan", "teal", "green", "lightgreen", "lime"];

        $scope.property = {};

        this.submit = function(property) {
            $scope.addItem(property)
            this.reset();
            $scope.propertyForm.$setPristine();
        }

        this.reset = function () {
            var d = new Date();
            this.property = {
                isRunning: false,
                lastUsed: d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds(),
                bgcolor: this.colors[Math.floor((Math.random() * 11))]
            };
        }
        this.reset();

    });


})();
